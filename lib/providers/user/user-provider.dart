
import 'package:car_care/models/user/user.dart';
import 'package:car_care/services/user-service.dart';
import 'package:flutter/foundation.dart';

class UserProvider extends ChangeNotifier {
  User user;

  void getUser(String str) async {
    user = await UserService.getUser(str);
    notifyListeners();
  }
  void removeUser(){
    user = null;
    notifyListeners();
  }
}
