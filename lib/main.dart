
import 'package:car_care/plugin/locator.dart';
import 'package:car_care/plugin/navigator.dart';
import 'package:car_care/providers/common/connectivity-provider.dart';
import 'package:car_care/providers/common/theme-provider.dart';
import 'package:car_care/providers/common/token-provider.dart';
import 'package:car_care/providers/user/user-provider.dart';
import 'package:car_care/router/router.dart';
import 'package:car_care/screens/splash/body.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:provider/provider.dart';
import 'config/env-config.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return Consumer<ThemeManager>(
      builder: (context, manager, child) {
        return new MaterialApp(
          title: 'CarCare',
          routes: RoutesConstant.routes,
          debugShowCheckedModeBanner: false,
          navigatorKey: locator<NavigationService>().navigatorKey,
          theme: manager.themeData,
          home: new BodySplash(),
        );
      },
    );
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  EnvConfig.setEnvironment(Environment.DEV);
  runApp(
    Phoenix(
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => UserProvider()),
          ChangeNotifierProvider(create: (context) => ThemeManager()),
          ChangeNotifierProvider(create: (context) => TokenProvider()),
          ChangeNotifierProvider(create: (context) => ConnectivityProvider()),
        ],
        child: MyApp(),
      ),
    ),
  );
}
