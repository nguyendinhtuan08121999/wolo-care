
import 'package:car_care/screens/editmenu/edit-menu.dart';
import 'package:car_care/screens/oderhistory/order-history.dart';
import 'package:car_care/screens/ordertable/order-table.dart';
import 'package:car_care/screens/profile/user-profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';

class DashboardHomePage extends StatefulWidget {
  DashboardHomePage({Key key}) : super(key: key);

  @override
  _DashboardHomePageState createState() => _DashboardHomePageState();
}

class _DashboardHomePageState extends State<DashboardHomePage> with SingleTickerProviderStateMixin {
  int _selectedIndex = 0;

  List listPage = [ OrderTableScreen(), EditMenuScreen(), OrderHistoryScreen(), UserProfile()];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: listPage.elementAt(_selectedIndex),
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
        ),
        child: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: buildIcon('images/coffee.svg', isActive: true),
              activeIcon: buildIcon('images/coffee-onclick.svg'),
              label: 'Order',
            ),
            BottomNavigationBarItem(
              icon: buildIcon('images/coffee-cup1.svg', isActive: true),
              activeIcon: buildIcon('images/coffee-cup1-onclick.svg',),
              label: 'Edit Menu',
            ),
            BottomNavigationBarItem(
              icon: buildIcon('images/coffee-cup.svg', isActive: true),
              activeIcon: buildIcon('images/coffee-cup-onclick.svg',),
              label: 'Order History',
            ),
            BottomNavigationBarItem(
              icon: buildIcon('images/profile-user.svg', isActive: true),
              activeIcon: Padding(
                padding: const EdgeInsets.symmetric(vertical: 3,),
                child: SizedBox(
                  height: 24,
                  width: 24,
                  child: SvgPicture.asset(
                    'images/profile-user.svg',
                    color: Colors.brown,
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              label: 'Profile',
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.brown,
          unselectedItemColor: Theme.of(context).disabledColor,
          type: BottomNavigationBarType.fixed,
          onTap: _onItemTapped,
          elevation: 1,
          showUnselectedLabels: true,
          selectedFontSize: 10,
          unselectedFontSize: 10,
          backgroundColor: Theme.of(context).backgroundColor,
        ),
      ),
    );
  }

  Widget buildIcon(String path, {bool isActive = false}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3,),
      child: SizedBox(
        height: 24,
        width: 24,
        child: SvgPicture.asset(
          path,
          color: isActive ? Theme.of(context).disabledColor : null,
          fit: BoxFit.contain,
        ),
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
