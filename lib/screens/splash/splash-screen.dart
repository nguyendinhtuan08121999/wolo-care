
import 'package:car_care/providers/common/connectivity-provider.dart';
import 'package:car_care/router/routing-name.dart';
import 'package:car_care/styles/component.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with SingleTickerProviderStateMixin {
  ConnectivityResult oldConnectivityStatus;
  bool isNotConnected = false;

  @override
  void initState() {
    super.initState();
  }

  Future onConnected() async {
    _loadNextScreen();
  }

  _loadNextScreen() async {
    bool isLogged = false;
    await Future.delayed(Duration(seconds: 3));
    if (!isLogged) {
      Navigator.pushNamedAndRemoveUntil(context, RoutingNameConstant.loginScreen, (Route<dynamic> route) => false);
    } else {
      //Provider.of<UserProvider>(context, listen: false).getUser();
      Navigator.pushNamedAndRemoveUntil(context, RoutingNameConstant.homeRoute, (Route<dynamic> route) => false);
    }
  }

  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                'images/logo-coffee.png',
                //fit: BoxFit.contain,
              ),
              SizedBox(height: 20,),
              Text('Car Care', style: CommonTextStyle.size20Bold.copyWith(color: Colors.brown, fontSize: 30),)
            ],
          ),
        ),
        Consumer<ConnectivityProvider>(
          builder: (context, connectivityProvider, child) {
            var connectivityStatus = connectivityProvider.connectivityResult;
            if (connectivityStatus == null) {
              return SizedBox();
            }
            var isConnect = connectivityStatus != ConnectivityResult.none;
            if (!isConnect) {
              isNotConnected = true;
            }
            if (oldConnectivityStatus != connectivityStatus && isConnect) {
              onConnected();
            }
            oldConnectivityStatus = connectivityStatus;
            return Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                isNotConnected
                  ? Center(
                    child: SizedBox(
                      height: 26,
                      width: 26,
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                      ),
                    )
                )
                : SizedBox(),
                const SizedBox(height: 20),
                !isConnect
                  ? Text(
                    'Please check your connection!',
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 14,
                      fontWeight: FontWeight.w300,
                    ),
                  )
                  : SizedBox(),
                const SizedBox(height: 100),
              ],
            );
          },
        ),
      ],
    );
  }
}
