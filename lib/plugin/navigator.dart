import 'package:car_care/config/constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import '../styles/component.dart';

class NavigationService {
  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

  Future<dynamic> navigateTo(String routeName) {
    return navigatorKey.currentState.pushNamed(routeName);
  }

  Future<dynamic> navigatePushAndRemoveUntil(String routeName) {
    return navigatorKey.currentState.pushNamedAndRemoveUntil(routeName, (route) => false);
  }

  BuildContext getContext() {
    return navigatorKey.currentState.overlay.context;
  }

  void showNetwork() async {
    if (Constant.isShowingDialog) {
      return;
    }
    Constant.isShowingDialog = true;
    var context = navigatorKey.currentState.overlay.context;
    Widget okButton = FlatButton(
      child: Text('Retry'),
      onPressed: () async {
        Phoenix.rebirth(context);
      },
    );

    showDialog(
      context: context,
      builder: (context) => Center(
        child: Material(
          color: Colors.transparent,
          child: AlertDialog(
            title: Text('Network error',),
            content: Text('Please check connection.'),
            actions: [
              okButton,
            ],
          ),
        ),
      ),
    );
  }
}
