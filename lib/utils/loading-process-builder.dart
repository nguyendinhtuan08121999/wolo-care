import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

abstract class LoadingProcessBuilder {
  static void showProgressDialog(BuildContext context, String title) {
    try {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Theme.of(context).backgroundColor,
            content: SizedBox(
              width: MediaQuery.of(context).size.width - 10,
              height: 120,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  CircularProgressIndicator(strokeWidth: 3,),
                  const SizedBox(height: 20),
                  Text(
                    title,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
          );
        });
    } catch (e) {
      print(e.toString());
    }
  }
}
