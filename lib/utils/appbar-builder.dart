import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class AppbarBuilder {

  static Widget primaryAppbar(String str, [bool check = false, BuildContext context]) {
    return AppBar(
      centerTitle: true,
      elevation: 0,
      toolbarHeight: 80,
      automaticallyImplyLeading: false,
      title: Text(str, textAlign: TextAlign.center,),
      actions: [
        SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20,),
            child: Image.asset(
              'images/logo-coffee.png',
              fit: BoxFit.contain,
            )
          ),
        ),
      ],
      leading: check
        ? IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.grey,),
          onPressed: () {
            Navigator.pop(context);
          }
        ) : null
    );
  }

}
