class TokenObj {
  String accessToken;

  TokenObj({this.accessToken = ''});

  factory TokenObj.fromJson(str) {
    return TokenObj(accessToken: str);
  }
}
