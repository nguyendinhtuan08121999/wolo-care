import 'package:json_annotation/json_annotation.dart';

part 'pagination.g.dart';

@JsonSerializable()

class Pagination {
  int page;
  int size;
  int total;
  int totalRecords;

  Pagination(this.page, this.size, this.totalRecords);

  factory Pagination.fromJson(Map<String, dynamic> json) => _$PaginationFromJson(json);

  Map<String, dynamic> toJson() => _$PaginationToJson(this);

}
