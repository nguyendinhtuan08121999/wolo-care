class TypeOrder {
  String name;

  String value;

  TypeOrder({this.name = '', this.value = ''});

  factory TypeOrder.fromJson(Map<String, dynamic> json) {
    return TypeOrder(name: json['name'], value: json['value']);
  }
}
