class User {

  String displayName;

  String username;

  String role;

  User({ this.displayName, this.username, this.role});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      displayName: json['displayName'],
      username: json['username'],
      role: json['role'],
    );
  }

}
