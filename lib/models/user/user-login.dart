class UserLoginForm {

  String username;

  String password;

  UserLoginForm({ this.username, this.password});

  factory UserLoginForm.fromJson(Map<String, dynamic> json) {
    return UserLoginForm(
      username: json['username'],
      password: json['password'],
    );
  }
  Map<String, dynamic> toJson() =>
    {
      'username': username,
      'password': password,
    };
}
