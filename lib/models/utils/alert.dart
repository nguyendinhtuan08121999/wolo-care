class AlertModel {

  String title;

  String description;

  String image;

  AlertModel({ this.title, this.description, this.image });

}
