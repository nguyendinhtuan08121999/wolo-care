
import 'package:car_care/router/routing-name.dart';
import 'package:car_care/screens/layout/main.dart';
import 'package:car_care/screens/sign-in/sign_in_screen.dart';
import 'package:car_care/screens/splash/body.dart';
import 'package:flutter/material.dart';

abstract class RoutesConstant {
  static final routes = <String, WidgetBuilder> {
    RoutingNameConstant.homeRoute: (BuildContext context) => new DashboardHomePage(),
    RoutingNameConstant.splashScreen: (BuildContext context) => new BodySplash(),
    RoutingNameConstant.loginScreen: (BuildContext context) => new SignInScreen(),
  };
}
