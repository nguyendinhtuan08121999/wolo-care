
import 'package:car_care/config/constant.dart';
import 'package:car_care/plugin/dio.dart';
import 'package:dio/dio.dart';

class AuthService {

  static Future<Response> login(Map data) async {
    try {
      var response = await http.post(Constant.apiUrl + 'login', data: data);
      return response;
    } catch (e) {
      return null;
    }
  }
}
