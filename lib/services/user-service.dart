

import 'package:car_care/config/constant.dart';
import 'package:car_care/models/user/user.dart';
import 'package:car_care/plugin/dio.dart';
import 'package:car_care/utils/shared-preference.dart';

abstract class UserService {
  static Future<User> getUser(String str) async {
    try {
      final response = await http.get(Constant.apiUrl + 'admin/taikhoan/$str');
      Map<String, dynamic> map = response.data;
      print('reponse: $response');
      User user = User.fromJson(map);
      await SharedPrefsService.setUser(map);
      return user;
    } catch (e) {
      return null;
    }
  }

  // static Future<Response> sendVerificationCode(dynamic payload) async {
  //   try {
  //     final response = await http.post(Constant.apiUrl + 'user/send-verification-code', data: payload);
  //     return response;
  //   } catch (e) {
  //     return null;
  //   }
  // }
}
