
import 'env-config.dart';

class Constant {
  static final apiUrl = EnvConfig.apiUrl;

  static final clientId = '';

  static bool isShowingDialog = false;

  static int timeReload = 0;

  static int lastTabSelect = 0;

  static int successText = 200;

  static String formatDateHour = 'dd/MM/yyyy\nHH:mm';

  static String formatDate = 'dd/MM/yyyy';

  static String formatDateHour1 = 'HH:mm-dd/MM/yyyy';

  static String formatMonth = 'MM/yyyy';

}
